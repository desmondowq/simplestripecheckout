<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $result = \App\Product::all();
    return view('shopping')->with('products', $result);
});

Route::get('carts/checkout', 'CartsController@checkout');



Route::get('payment/receipt/{orderNo}', 'PaymentController@receipt');

Route::post('payment/stripe', 'PaymentController@stripe');

Route::group(['prefix' => 'api/v1'], function (){
//    Route::resource('carts', 'CartsController');
    Route::post('carts/addToCart', 'CartsController@add_to_cart');
});

