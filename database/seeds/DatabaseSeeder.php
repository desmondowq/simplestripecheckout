<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProductTableSeeder::class);
        $this->command->info('Product table seeded!');

    }


}

class ProductTableSeeder extends  seeder
{
    public  function  run(){

        App\Product::truncate();

        App\Product::create([
            'name' => 'Kindle Paperwhite',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam pellentesque velit eget nunc lobortis placerat. Duis molestie ultrices magna fermentum congue. Donec interdum vitae metus sit amet commodo. Vivamus porta diam quis dapibus tincidunt. Phasellus vel eros tellus.',
            'unit_price' => '100.00',
            'filename' => 'kindle.jpg'
        ]);
        App\Product::create([
            'name' => 'iPad',
            'description' => 'Sed eu gravida neque. Vestibulum ut mi neque. Etiam at lectus eros. In dui mi, venenatis et mi quis, viverra rhoncus ligula. Nulla eu nulla tristique, facilisis eros et, dictum tellus. Nulla finibus porta sapien quis vulputate. ',
            'unit_price' => '500.00',
            'filename' => 'ipad.jpg'
        ]);
        App\Product::create([
            'name' => 'iPhone',
            'description' => 'Vivamus posuere est orci. Nunc aliquet dignissim nisi. Nulla facilisi. Mauris vitae lobortis massa. Duis accumsan elit quis magna porta posuere. Nulla fermentum dui non augue blandit, vitae lobortis eros imperdiet. Nam consectetur ante a congue bibendum.',
            'unit_price' => '1888.00',
            'filename' => 'iphone.jpg'
        ]);
    }
}
