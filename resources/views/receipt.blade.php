@extends('layouts.main')

@section('css')
    <style>

    </style>
@endsection


@section('content')
    <h1 class="text-center">Paid</h1>
    <p class="text-center">
        Here is your Receipt
    </p>
    <center>
        <?php $total_price = 0; ?>

        <p><strong>Order No: {{$order->orderNo}}</strong></p>
        @foreach($order->orderItems as $item)
            <p>{{$item->qty}} x {{$item->product->name}} - ${{number_format($item->unit_price,2)}}</p>
            <?php $total_price += $item->product->unit_price * $item->qty; ?>
            @endforeach
        <p><strong>Total: ${{number_format($total_price, 2)}}</strong></p>
    </center>

@endsection