@extends('layouts.main')

@section('css')
    <style>
        .item-row{
            padding: 10px 0;
        }
        .item-qty{
            width: 50px;
        }
        input[type=number]::-webkit-inner-spin-button,
        input[type=number]::-webkit-outer-spin-button {
            opacity: 1;
        }
    </style>
@endsection


@section('content')
    <div class="row">
        <h1>Shopping Page</h1>
    </div>

    @foreach($products as $product)
        <div class="row item-row">
            <div>
                <img src="{{ asset('images').'/'.$product->filename }}" height="200" width="200" />
            </div>
            <div>
                <p>{{$product->name}} ${{$product->unit_price}}</p>
                <p><button onclick="addToCart({{$product->id}})" >Add to Cart</button></p>
            </div>
        </div>
    @endforeach
    <div class="row">
        <a href="{{ action("CartsController@checkout") }}" class="btn btn-primary offset-md-2">Checkout</a>
    </div>
@endsection
@section('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        function addToCart (product_id) {
            $.post("api/v1/carts/addToCart", {product_id: product_id}, function(data, status){
                swal("Item added to cart!");
            });
        }
    </script>

@endsection