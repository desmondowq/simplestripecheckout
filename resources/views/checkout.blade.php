@extends('layouts.main')

@section('css')
    <style>
        .pull-right{
            float: right;
        }
        .item-description{
            text-align: justify ;
        }
    </style>
@endsection


@section('content')
    @if(Session::has('payment-error-message'))
        <p class="alert alert-danger">{{ Session::get('payment-error-message') }}</p>
    @endif
    @if(count($items) > 0)
        <div class="row">
            <h1>Checkout Page</h1>
        </div>
        <?php $total_price = 0; ?>
        @foreach($items as $item)
            <div class="media">
                <img class="mr-3" src="{{ asset('images').'/'.$item->product->filename }}" width="256" alt="{{$item->product->name}}">
                <div class="row">
                    <div class="col-md">
                        {{$item->product->name}}
                    </div>
                    <div class="col-md">
                        <strong class="pull-right">Qty: {{$item->qty}}</strong>
                    </div>
                    <div class="col-md">
                        <strong class="pull-right">${{number_format($item->product->unit_price * $item->qty, 2)}}</strong>
                    </div>
                    <div class="w-100"></div>
                    <div class="col-md item-description">
                        <br/>
                        {{$item->product->description}}
                    </div>
                </div>
            </div>
            <?php $total_price += $item->product->unit_price * $item->qty; ?>
        @endforeach

        <div class="pull-right">
            <p><strong>Total: ${{number_format($total_price, 2)}}</strong></p>
            <form class="pull-right" action="{{ action("PaymentController@stripe") }}" method="POST">
                {{ csrf_field() }}
                <script
                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                        data-key="pk_test_ADbX3J0z7zDpdWMPuztkLAiJ"
                        data-name="Shopping"
                        data-description="Widget"
                        data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                        data-locale="auto"
                        data-zip-code="true"
                        data-email="true"
                        data-label="Pay"
                        data-allow-remember-me="false"
                        data-shipping-address="true">
                </script>
            </form>
        </div>
    @else
        <center>
            <h1>Start buying by seleting <a href="{{url('/')}}">here</a></h1>

        </center>

    @endIf


@endsection

@section('scripts')


@endsection