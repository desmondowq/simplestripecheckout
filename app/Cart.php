<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected  $fillable = [
        'name',
        'description',
        'qty'
    ];
    //
    public function product(){
        return $this->belongsTo('App\Product');
    }
}
