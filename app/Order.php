<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    //for mass assignment

    protected  $fillable = [
        'OrderNo',
        'name',
        'email',
        'country',
        'address'
    ];

    public function OrderItems()
    {
        return $this->hasMany('App\OrderItem');

    }
}
