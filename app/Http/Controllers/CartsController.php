<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Support\Facades\Input;

class CartsController extends Controller
{
    function __construct() {
        parent::__construct();
    }

    /**
     *  Get cart item
     */
    public function index(){

        $items = Cart::where('session_key', $this->session_key)->get();

        return response()->json([
            'data' => $this->transformCollection($items)
        ], 200) ;
    }

    /**
     *
     * Add selected item to user cart, tracked by session_key
     *
     * @param int product_id
     *
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */

    public function add_to_cart(){
        if(!Input::get('product_id')){
           //return error status code
            return response('Unable to add item to cart', 400);
        }

        //check if product exist
        if (Product::where('id', '=', Input::get('product_id'))->count() == 0) {
            //return error status code
            return response('Unable to add item to cart', 400);
        }

        //create or update qty of product in cart
        $cart = Cart::firstOrNew(array('product_id' => Input::get('product_id'),
            'session_key' => $this->session_key));
        $cart->product_id = Input::get('product_id');
        $cart->session_key = $this->session_key;
        $cart->qty += 1;
        $cart->save();


        return response()->json([
            'message' => 'successfully added product to cart'
        ], 200) ;
    }

    /**
     *
     * Checkout page, get all selected product from the cart table
     *
     */

    public function checkout(){

        $items = Cart::where('session_key', $this->session_key)->get();

        return view('checkout')
            ->with('items', $items);
    }

    /*
     *
     *  transform of model to return appropriate data
     *
     */

    private function transformCollection($items){
        return array_map([$this, 'transform'], $items->toArray());
    }



    private function  transform($item)
    {
        return[
            'product_id' => $item['product_id'],
            'qty' => (integer) $item['qty']
        ];
    }


}
