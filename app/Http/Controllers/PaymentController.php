<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Stripe\Order;
use Session;
use Stripe_Error;

class PaymentController extends Controller
{
    function __construct() {
        parent::__construct();
    }

    /**
     *
     * Set purchase description before executing stripe charge using stripeToken,
     * On success insert purchase record into database,
     * Order for customer detail
     * Order item for the product that the user purchase
     *
     * Request: data return from stripe example
     * @param string stripeToken
     * @param string stripeTokenType
     * @param string stripeEmail
     * @param string stripeBillingName
     * @param string stripeBillingAddressCountry
     * @param string stripeBillingAddressCountryCode
     * @param string stripeBillingAddressZip
     * @param string stripeBillingAddressLine1
     * @param string stripeBillingAddressCity
     * @param string stripeShippingName
     * @param string stripeShippingAddressCountry
     * @param string stripeShippingAddressCountryCode
     * @param string stripeShippingAddressZip
     * @param string stripeShippingAddressCity
     *
     */
    public function stripe(Request $request){
        \Stripe\Stripe::setApiKey('sk_test_1rgfm7UQOI97X9QROqEWyOg8');


        $description = '';
        $items =  \App\Cart::where('session_key', $this->session_key)->get();
        $total_price = 0;
        foreach ($items as $item){
            if($description !== '')
            {
                $description .= ', ';

            }
            $description .= $item->qty . ' x ' . $item->product->name . ' at ' . number_format($item->product->unit_price, 2);
            $total_price += $item->qty * $item->product->unit_price;
        }
        $description .= '.';

        $total_price *= 100;

        try {
            //charge
            $jsonCharge = \Stripe\Charge::create ( array (
                "amount" => ceil($total_price),
                "currency" => "sgd",
                "source" => $request->input ( 'stripeToken' ), // obtained with Stripe.js
                "description" => $description
            ));
            //create order
            $order = new  \App\Order;
            $order->orderNo =  $jsonCharge->id;
            $order->name = $request->input ( 'stripeShippingName' );
            $order->email = $request->input ( 'stripeEmail' );
            $order->country = $request->input ( 'stripeShippingAddressCountry' );
            $order->address = $request->input ( 'stripeShippingAddressLine1' );
            $order->save();

            foreach ($items as $item){
                $orderItem = new  \App\OrderItem;
                $orderItem->qty = $item->qty;
                $orderItem->unit_price = $item->product->unit_price;
                $orderItem->order_id = $order->id;
                $orderItem->product_id = $item->product_id;
                $orderItem->save();

                $item->delete();
            }

            return redirect()->action(
                'PaymentController@receipt', ['orderNo' => $order->orderNo]
            );

        } catch ( \Stripe\Error\Base  $e ) {
            session::flash ( 'payment-error-message', "Error! ". $e->getMessage() ." Please Try again. " );
            return redirect()->action(
                'CartsController@checkout'
            );
        }catch (Exception $e) {
            session::flash ( 'payment-error-message', "Error! Please Try again." );
            return redirect()->action(
                'CartsController@checkout'
            );
        }
    }


    /*
     * users purchase receipt
     */
    public function receipt($orderNo){
        $order = \App\Order::where('orderNo', $orderNo)->first();

        return view('receipt')
            ->with('order', $order);
    }
}
