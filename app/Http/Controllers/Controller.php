<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
//    protected $session_key;

    /**
     * Controller constructor.
     *
     * set user session key, for adding of cart and purchasing tracking.
     *
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next)
        {
            // Retrieve a piece of data from the session...
            $this->session_key = session('key');
            // Specifying a default value...
            $this->session_key = session('key', uniqid());

            // Store a piece of data in the session...
            session(['key' => $this->session_key ]);
            return $next($request);
        });

    }
}
