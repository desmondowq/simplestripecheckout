<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    //for mass assignment

    protected  $fillable = [
        'session_key',
        'product_id',
        'qty'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }

    public function order(){
        return $this->belongsTo('App\Order');
    }

}
