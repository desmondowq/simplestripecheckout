<?php
/**
 * Created by PhpStorm.
 * User: daski
 * Date: 12/11/2017
 * Time: 12:43 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';

    //
    //for mass assignment

    protected  $fillable = [
        'qty',
        'unit_price',
        'order_id',
        'product_id'
    ];

    public function product(){
        return $this->belongsTo('App\Product');
    }
}
