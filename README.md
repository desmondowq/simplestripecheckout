# Project Title

Simple stripe checkout

## Getting Started

Install Nodejs and composer 
using the follow
````
Nodejs: https://nodejs.org/en/
Composer: https://getcomposer.org/
```` 

using terminal locate your prefer http server to clone the respository
````
$ git clone https://bitbucket.org/desmondowq/simplestripecheckout.git
$ cd simplestripecheckout
$ composer install
````

##Migration
Create local database and edit the detail under .env locate in the root folder

Example
````
DB_CONNECTION=mysql
DB_HOST={host}
DB_PORT=3306
DB_DATABASE={database name}
DB_USERNAME={username}
DB_PASSWORD={password}
````

Type in the following cmdlet in your current directory
````
$ php artisan migrate --seed
````
This will create 6 tables and fill in 3 default product into the product table.

##Entity Relationship Diagram
![alt text](https://bytebucket.org/desmondowq/simplestripecheckout/raw/b8d8debf9add12a38fe6e044975015a8800be960/public/images/stripe%20checkout.png)





